import React, {useState} from 'react';
import './App.css';
import Article from './Article';

// Ecma Script 6 (ES6)
const Coba2 = () => {
  const [number, setNumber ] = useState(0);

  const handleTambah = () => {
    setNumber(number + 1);
  }

  function handleKurang () {
    setNumber(number <= 0 ? 0 : number - 1);
  }

  return (
    <div>
      <p>Hasil Tambahan, {number}</p>
      <button onClick={handleTambah}>Tambah</button>
      <button onClick={handleKurang}>Kurang</button>
    </div>
  )
}

// Ecma Script 5 (ES5)
function Coba1 () {
  return (
    <div>
      <div>
        <p>coba123</p>
        <p>coba444</p>
      </div>
    </div>
  )
}

function App () {
  return (
    <div>
      <p>test1</p>
      <Coba1 />
      <Coba2 />
      <Article />
    </div>
  )
}

export default App;
