import React from 'react';

class Article extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            count : 1,
            isCounted : false
        };

        this.handleChangeColor = this.handleChangeColor.bind(this);
    }

    handleChangeColor(){
        this.setState({
            isCounted : true,
            count : this.state.count + 1
        })

    }

    render() {
      console.log('value state isCounted', this.state.isCounted);

      return  (
          <div>
              <h1>Hello, {this.state.count}</h1>
              <button onClick={this.handleChangeColor}>Change Color</button>
          </div>
      )
    }
  }

export default Article;

